<?php
  
function photo_blog_preprocess_page(&$variables){  //& means not returning anything
  
 // print_r($variables);  prints all variables out
    $page = $variables["page"];
    $body_class = "full-width";
    $left_column_class = "";
    $right_column_class = "";

    if (!empty($page["right_column"]) && !empty($page["left_column"])) {
        $left_column_class = "one-fourth";
        $right_column_class = "one-fourth";
        $body_class = "half";
    } else if (!empty($page["left_column"]) && !empty($page["content"])) {
        $left_column_class = "one-fourth";
        $body_class = "three-fourths";
    } else {
        $right_column_class = "one-fourth";
        $body_class = "three-fourths";
    }

    $variables["photo_blog"]["body_class"] = $body_class;
    $variables["photo_blog"]["right_column_class"] = $right_column_class;
    $variables["photo_blog"]["left_column_class"] = $left_column_class;
  
}
