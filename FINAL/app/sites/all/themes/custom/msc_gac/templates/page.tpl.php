<div class="site-container">
    
    <!-- header -->
    <div class="header-wrapper">
        <?php if($page['header']): ?>
			<div class="header">
					<?php print render($page['header']); ?>
			</div>
        <?php endif; ?>  
        <?php if ($logo): ?>
            <div class="logo">
                <a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" /></a>
            </div>
        <?php endif; ?>  
    </div>

    <!-- navigation -->
    <div class="navigation-container">
        <div class="menu-container">
            <?php if($page['main_menu']): ?>
			    <div class="main_menu">
					<?php print render($page['main_menu']); ?>
			    </div>
            <?php endif; ?> 
        </div>
    </div>
    
		
	<!-- above content -->
	<div class="above-content-container">
		<?php if($page['above_content']): ?>
			<div class="above-content">
				<?php print render($page['above_content']); ?>
			</div>
		<?php endif; ?>
	</div>
   
    
    <!-- content -->
    <div class="content-container">
        <!-- view and edit tabs here -->
        <div class="tab-container">
            <?php if ($tabs): ?>
                  <?php print render($tabs); ?>
            <?php endif; ?>
        </div>

        <div class="title">
            <?php if ($title == "Ice Cream Products" || $title == "Cookie Products") : ?><h2 class="title"><?php print $title; ?></h2><?php endif; ?> 
        </div>
       
        <div class="content clearfix">
			<div class="left-column column region left <?php print $variables["msc_gac"]["left_column_class"]?>">
                <?php if($page['left_column']): ?>
                    <?php print render($page['left_column']); ?>
                <?php endif; ?>
			</div>
            <div class="main-content left <?php print $variables["msc_gac"]["body_class"]?>">
                <div class="content">
                    
                    <!-- messages will go here -->
                    <?php if ($messages): ?>
                        <div id="messages">
                            <div class="section clearfix">
                                <?php print $messages; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    
                  <?php print render($page['content']); ?>
                </div>
            </div>
            <div class="right-column column region left <?php print $variables["msc_gac"]["right_column_class"]?>">
                <?php if($page['right_column']): ?>
                    <?php print render($page['right_column']); ?>
                <?php endif; ?>
            </div>
        </div>

        <!-- below content -->
        <div class="below-content-container">
            <?php if($page['below_content']): ?>
                <div class="below-content">
                    <?php print render($page['below_content']); ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
    
    <!-- footer -->
    <div class="footer-container">
        <div>  
            <ul>
                <li>MARBLE SLAB &amp; <br>GREAT AMERICAN COOKIES</li>
                <li>&#x000A9; 2018</li>
            </ul> 
            <ul>
                <li>2310 SE Delaware Ave Suite O</li>
                <li>Ankeny, IA 50021</li>
                <li>(515) 964-9300</li>
            </ul>
            <ul>
                <li><a href="https://www.facebook.com/Marble-Slab-Creamery-117070161644532/" target="_blank" ><img src="<?php print base_path() . path_to_theme() . '/' . 'img/facebookIcon.png'; ?>" width="32px"></a></li>
            </ul>
        </div>
    </div>
    
</div>
