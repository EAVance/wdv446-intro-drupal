<?php
  
function msc_gac_preprocess_page(&$variables){  //& means not returning anything
  
 // print_r($variables);  prints all variables out
    $page = $variables["page"];
    $body_class = "full-width";
    $left_column_class = "";
    $right_column_class = "";

    if (!empty($page["right_column"]) && !empty($page["left_column"])) {
        $left_column_class = "one-fourth";
        $right_column_class = "one-fourth";
        $body_class = "half";
    } else if (!empty($page["left_column"]) && !empty($page["content"])) {
        $left_column_class = "one-fourth";
        $body_class = "three-fourths";
    } else if (!empty($page["right_column"]) && !empty($page["content"])) {
        $right_column_class = "one-fourth";
        $body_class = "three-fourths";
    } 

    $variables["msc_gac"]["body_class"] = $body_class;
    $variables["msc_gac"]["right_column_class"] = $right_column_class;
    $variables["msc_gac"]["left_column_class"] = $left_column_class;
  
}
